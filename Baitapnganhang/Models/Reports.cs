﻿namespace Baitapnganhang.Models
{
    public class Reports
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
        public int AccountsId { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsId { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionsId { get; set; }
        public Transactions? Transactions { get; set; }
    }
}
