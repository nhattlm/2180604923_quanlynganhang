﻿namespace Baitapnganhang.Models
{
    public class Transactions
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public Employees? Employees { get; set; }
        public Customer? Customer { get; set; }
        public List<Reports>? Reports { get; set; }
        public List<Logs>? Logs { get; set; }
    }
}
