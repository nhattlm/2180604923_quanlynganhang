﻿using System.ComponentModel.DataAnnotations;

namespace Baitapnganhang.Models
{
    public class Customer
    {
        public int Id { get; set; }
        [Required, StringLength(100)]
        public string FisrtName { get; set; }
        [Required, StringLength(100)]
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public List<Transactions>? Transactions { get; set; }
        public List<Accounts>? Accounts { get; set; }

    }
}
