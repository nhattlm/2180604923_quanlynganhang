﻿using Baitapnganhang.Models;
using Microsoft.EntityFrameworkCore;

namespace Baitapnganhang.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options)
        {
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    // Thêm lệnh SQL để thay đổi ràng buộc foreign key
        //    modelBuilder.Entity<Reports>()
        //        .HasOne(r => r.Transactions)
        //        .WithMany()
        //        .HasForeignKey(r => r.TransactionsId)
        //        .OnDelete(DeleteBehavior.ClientSetNull);
        //}


    }
}